// Frequency bands we are interested in, in increasing order of frequency
var bands = [
	{ "label": "160m", "start": 1.800, "end": 2.000 },
	{ "label": "80m", "start": 3.500, "end": 4.000 },
	{ "label": "60m", "start": 5.3305, "end": 5.4065 },
	{ "label": "40m", "start": 7.000, "end": 7.300 },
	{ "label": "30m", "start": 10.100, "end": 10.150 },
	{ "label": "20m", "start": 14.000, "end": 14.350 },
	{ "label": "17m", "start": 18.068, "end": 18.168 },
	{ "label": "15m", "start": 21.000, "end": 21.450 },
	{ "label": "12m", "start": 24.890, "end": 24.990 },
	{ "label": "10m", "start": 28.000, "end": 29.700 },
	{ "label": "6m", "start": 50.000, "end": 54.000 },
];

// Set it all up
function on_load() {
	create_inputs();
	update_canvas();
}

// Creat the input checkboxes for each band
function create_inputs() {
	var form = document.getElementById("input");
	for (let band of bands) {
		var label = document.createElement("label");
		var checkbox = document.createElement("input");
		checkbox.type = "checkbox";
		checkbox.onchange = "update_canvas()";
		var label_text = band.label + " (" + band.start + "-" +
				band.end + "MHz)";
		var text_node = document.createTextNode(label_text);
		label.appendChild(checkbox);
		label.appendChild(text_node);
		form.appendChild(label);
		band.checkbox = checkbox;
	}
}

// Redraw the canvas
function update_canvas() {
	var canvas = document.getElementById("output");
	var ctx = canvas.getContext("2d");

	// Clear the screen
	ctx.setTransform(1,0,0,1,0,0);
	ctx.clearRect(0,0,canvas.width,canvas.height);

	// Fetch the bands that are enabled
	// If there are none, we don't need to continue
	const enabled = bands.filter(band => band.checkbox.checked);
	if (enabled.length == 0) {
		return;
	}

	// Compute the range
	const lowest_halfwave = 468 / enabled[0].start;
	const start_ft = Math.floor(0.5*lowest_halfwave/5)*5;
	const end_ft = Math.ceil(3*lowest_halfwave/5)*5;

	// Adjust the width of the canvas
	canvas.width = (end_ft - start_ft) * 10 + 40;

	// Need to save after resetting width, which resets all drawing state
	ctx.save();

	// Draw a scale at the bottom
	draw_scale(ctx, start_ft, end_ft, canvas.height);

	// Translate and scale coordinates so x axis is feet and y axis is
	// like Cartesian from -1 to 1.
	ctx.scale(10, -(canvas.height-25)/2);
	ctx.translate(-start_ft, -1);

	// Plot the impedance chart
	plot_impedance(ctx, enabled, start_ft, end_ft);

	// Finally, restore original state
	ctx.restore();
}

// Draw a scale to indicate the length of the antenna
function draw_scale(ctx, start, end, height) {
	// Some constants for how we want to draw
	const pixels_per_foot = 10;
	const label_step = 5;
	const font_size = 10;
	const label_height = font_size + 2;
	const scale_height = 10;
	const small_scale = scale_height/2;

	ctx.save();

	// Translate to the bottom of the canvas
	ctx.translate(0, height);

	// Set the font
	ctx.font = font_size + "pt serif";

	// Set the line styling
	ctx.lineWidth = 2;

	// For each section of 5 feet
	for (var i = start; i <= end; i += label_step) {
		// Compute the current x position
		var x = pixels_per_foot * (i - start);

		// Draw the text label
		ctx.fillText(i, x, 0);

		// Start a path for drawing the lines
		ctx.beginPath();

		// Draw the larger scale line next to the label
		// keep in mind that up is negative!
		ctx.moveTo(x, -label_height - scale_height);
		ctx.lineTo(x, -label_height);

		// Only draw forward if this isn't the last label
		if (i < end) {
			// Create some variables to track where we are
			var cx = x + pixels_per_foot;
			var cy = -label_height;

			// Draw the first horizontal step
			ctx.lineTo(cx, cy);

			// For each 1-foot marker
			for (var j = 0; j < 4; ++j) {
				// Draw the smaller vertical 1-foot marker
				ctx.lineTo(cx, cy);
				ctx.lineTo(cx, cy - small_scale);
				ctx.lineTo(cx, cy);
				cx += pixels_per_foot;
			}

			// Draw the final connector to the next label
			ctx.lineTo(cx, cy);
		}
		ctx.stroke();
	}
	ctx.restore();
}

// Plot the impedance for the enabled bands
function plot_impedance(ctx, enabled, start, end) {
	// Start drawing
	ctx.fillStyle = "red";
	ctx.beginPath();
	ctx.moveTo(start, -1);

	for (var x = start; x <= end; x += 1/50) {
		ctx.lineTo(x, max_impedance(enabled, x));
	}

	ctx.lineTo(end, -1);
	ctx.closePath();
	ctx.fill();
}

// This function calculates the maximum impedance across bands
function max_impedance(bands, length) {
	var max = -1;
	for (const band of bands) {
		max = Math.max(max, band_impedance(band, length));
	}
	return max;
}

// Compute the max band impedance at a length
function band_impedance(band, length) {
	// We need to return 1 if the length is a half wavelength between the
	// start and end frequencies
	// The velocity factor could differ a bit, so use different constants
	// for the half wavelength at the start of the band and end of the band
	// to give some wiggle room
	var band_start_wl = 480 / band.start;
	var band_end_wl = 470 / band.end;
	var hw_start = band_start_wl;
	var hw_end = band_end_wl;
	var count = 1;
	while (length > hw_end) {
		if (length <= hw_start && length >= hw_end) {
			return 1;
		}
		count++;
		hw_start = band_start_wl * count;
		hw_end = band_end_wl * count;
	}
	return -1;
}

